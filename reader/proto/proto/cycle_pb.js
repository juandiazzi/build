// source: proto/cycle.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var proto_truck_pb = require('../proto/truck_pb.js');
goog.object.extend(proto, proto_truck_pb);
var proto_excavator_pb = require('../proto/excavator_pb.js');
goog.object.extend(proto, proto_excavator_pb);
var proto_material_pb = require('../proto/material_pb.js');
goog.object.extend(proto, proto_material_pb);
var proto_streaming_pb = require('../proto/streaming_pb.js');
goog.object.extend(proto, proto_streaming_pb);
var proto_load_pb = require('../proto/load_pb.js');
goog.object.extend(proto, proto_load_pb);
goog.exportSymbol('proto.pb.Cycle', null, global);
goog.exportSymbol('proto.pb.Cycle.Mode', null, global);
goog.exportSymbol('proto.pb.Cycle.Shift', null, global);
goog.exportSymbol('proto.pb.Cycle.State', null, global);
goog.exportSymbol('proto.pb.CycleRequest', null, global);
goog.exportSymbol('proto.pb.CycleResponse', null, global);
goog.exportSymbol('proto.pb.CyclesResponse', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.pb.Cycle = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.pb.Cycle.repeatedFields_, null);
};
goog.inherits(proto.pb.Cycle, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.pb.Cycle.displayName = 'proto.pb.Cycle';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.pb.CycleRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.pb.CycleRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.pb.CycleRequest.displayName = 'proto.pb.CycleRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.pb.CycleResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.pb.CycleResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.pb.CycleResponse.displayName = 'proto.pb.CycleResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.pb.CyclesResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.pb.CyclesResponse.repeatedFields_, null);
};
goog.inherits(proto.pb.CyclesResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.pb.CyclesResponse.displayName = 'proto.pb.CyclesResponse';
}

/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.pb.Cycle.repeatedFields_ = [19];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.pb.Cycle.prototype.toObject = function(opt_includeInstance) {
  return proto.pb.Cycle.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.pb.Cycle} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.pb.Cycle.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    operationId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    name: jspb.Message.getFieldWithDefault(msg, 3, ""),
    description: jspb.Message.getFieldWithDefault(msg, 4, ""),
    arrivalTime: jspb.Message.getFieldWithDefault(msg, 5, ""),
    startTime: jspb.Message.getFieldWithDefault(msg, 6, ""),
    endTime: jspb.Message.getFieldWithDefault(msg, 7, ""),
    number: jspb.Message.getFieldWithDefault(msg, 8, 0),
    distance: jspb.Message.getFloatingPointFieldWithDefault(msg, 9, 0.0),
    duration: jspb.Message.getFloatingPointFieldWithDefault(msg, 10, 0.0),
    mode: jspb.Message.getFieldWithDefault(msg, 11, 0),
    state: jspb.Message.getFieldWithDefault(msg, 12, 0),
    shift: jspb.Message.getFieldWithDefault(msg, 13, 0),
    upload: (f = msg.getUpload()) && proto_load_pb.Upload.toObject(includeInstance, f),
    download: (f = msg.getDownload()) && proto_load_pb.Download.toObject(includeInstance, f),
    material: (f = msg.getMaterial()) && proto_material_pb.Material.toObject(includeInstance, f),
    truck: (f = msg.getTruck()) && proto_truck_pb.Truck.toObject(includeInstance, f),
    excavator: (f = msg.getExcavator()) && proto_excavator_pb.Excavator.toObject(includeInstance, f),
    truckinfoList: jspb.Message.toObjectList(msg.getTruckinfoList(),
    proto_streaming_pb.TruckInfo.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.pb.Cycle}
 */
proto.pb.Cycle.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.pb.Cycle;
  return proto.pb.Cycle.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.pb.Cycle} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.pb.Cycle}
 */
proto.pb.Cycle.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setOperationId(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setArrivalTime(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setStartTime(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setEndTime(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setNumber(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setDistance(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setDuration(value);
      break;
    case 11:
      var value = /** @type {!proto.pb.Cycle.Mode} */ (reader.readEnum());
      msg.setMode(value);
      break;
    case 12:
      var value = /** @type {!proto.pb.Cycle.State} */ (reader.readEnum());
      msg.setState(value);
      break;
    case 13:
      var value = /** @type {!proto.pb.Cycle.Shift} */ (reader.readEnum());
      msg.setShift(value);
      break;
    case 14:
      var value = new proto_load_pb.Upload;
      reader.readMessage(value,proto_load_pb.Upload.deserializeBinaryFromReader);
      msg.setUpload(value);
      break;
    case 15:
      var value = new proto_load_pb.Download;
      reader.readMessage(value,proto_load_pb.Download.deserializeBinaryFromReader);
      msg.setDownload(value);
      break;
    case 16:
      var value = new proto_material_pb.Material;
      reader.readMessage(value,proto_material_pb.Material.deserializeBinaryFromReader);
      msg.setMaterial(value);
      break;
    case 17:
      var value = new proto_truck_pb.Truck;
      reader.readMessage(value,proto_truck_pb.Truck.deserializeBinaryFromReader);
      msg.setTruck(value);
      break;
    case 18:
      var value = new proto_excavator_pb.Excavator;
      reader.readMessage(value,proto_excavator_pb.Excavator.deserializeBinaryFromReader);
      msg.setExcavator(value);
      break;
    case 19:
      var value = new proto_streaming_pb.TruckInfo;
      reader.readMessage(value,proto_streaming_pb.TruckInfo.deserializeBinaryFromReader);
      msg.addTruckinfo(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.pb.Cycle.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.pb.Cycle.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.pb.Cycle} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.pb.Cycle.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getOperationId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getArrivalTime();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getStartTime();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getEndTime();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getNumber();
  if (f !== 0) {
    writer.writeInt64(
      8,
      f
    );
  }
  f = message.getDistance();
  if (f !== 0.0) {
    writer.writeDouble(
      9,
      f
    );
  }
  f = message.getDuration();
  if (f !== 0.0) {
    writer.writeDouble(
      10,
      f
    );
  }
  f = message.getMode();
  if (f !== 0.0) {
    writer.writeEnum(
      11,
      f
    );
  }
  f = message.getState();
  if (f !== 0.0) {
    writer.writeEnum(
      12,
      f
    );
  }
  f = message.getShift();
  if (f !== 0.0) {
    writer.writeEnum(
      13,
      f
    );
  }
  f = message.getUpload();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      proto_load_pb.Upload.serializeBinaryToWriter
    );
  }
  f = message.getDownload();
  if (f != null) {
    writer.writeMessage(
      15,
      f,
      proto_load_pb.Download.serializeBinaryToWriter
    );
  }
  f = message.getMaterial();
  if (f != null) {
    writer.writeMessage(
      16,
      f,
      proto_material_pb.Material.serializeBinaryToWriter
    );
  }
  f = message.getTruck();
  if (f != null) {
    writer.writeMessage(
      17,
      f,
      proto_truck_pb.Truck.serializeBinaryToWriter
    );
  }
  f = message.getExcavator();
  if (f != null) {
    writer.writeMessage(
      18,
      f,
      proto_excavator_pb.Excavator.serializeBinaryToWriter
    );
  }
  f = message.getTruckinfoList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      19,
      f,
      proto_streaming_pb.TruckInfo.serializeBinaryToWriter
    );
  }
};


/**
 * @enum {number}
 */
proto.pb.Cycle.State = {
  UNKNOWN_STATE: 0,
  OPENED: 1,
  CLOSED: 2
};

/**
 * @enum {number}
 */
proto.pb.Cycle.Mode = {
  UNKNOWN_MODE: 0,
  NORMAL: 1,
  ALTERED: 2
};

/**
 * @enum {number}
 */
proto.pb.Cycle.Shift = {
  UNKNOWN_SHIFT: 0,
  NIGHT: 1,
  DAY: 2
};

/**
 * optional string id = 1;
 * @return {string}
 */
proto.pb.Cycle.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string operation_id = 2;
 * @return {string}
 */
proto.pb.Cycle.prototype.getOperationId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setOperationId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string name = 3;
 * @return {string}
 */
proto.pb.Cycle.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string description = 4;
 * @return {string}
 */
proto.pb.Cycle.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string arrival_time = 5;
 * @return {string}
 */
proto.pb.Cycle.prototype.getArrivalTime = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setArrivalTime = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string start_time = 6;
 * @return {string}
 */
proto.pb.Cycle.prototype.getStartTime = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setStartTime = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string end_time = 7;
 * @return {string}
 */
proto.pb.Cycle.prototype.getEndTime = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setEndTime = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional int64 number = 8;
 * @return {number}
 */
proto.pb.Cycle.prototype.getNumber = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setNumber = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * optional double distance = 9;
 * @return {number}
 */
proto.pb.Cycle.prototype.getDistance = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 9, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setDistance = function(value) {
  return jspb.Message.setProto3FloatField(this, 9, value);
};


/**
 * optional double duration = 10;
 * @return {number}
 */
proto.pb.Cycle.prototype.getDuration = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 10, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setDuration = function(value) {
  return jspb.Message.setProto3FloatField(this, 10, value);
};


/**
 * optional Mode mode = 11;
 * @return {!proto.pb.Cycle.Mode}
 */
proto.pb.Cycle.prototype.getMode = function() {
  return /** @type {!proto.pb.Cycle.Mode} */ (jspb.Message.getFieldWithDefault(this, 11, 0));
};


/**
 * @param {!proto.pb.Cycle.Mode} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setMode = function(value) {
  return jspb.Message.setProto3EnumField(this, 11, value);
};


/**
 * optional State state = 12;
 * @return {!proto.pb.Cycle.State}
 */
proto.pb.Cycle.prototype.getState = function() {
  return /** @type {!proto.pb.Cycle.State} */ (jspb.Message.getFieldWithDefault(this, 12, 0));
};


/**
 * @param {!proto.pb.Cycle.State} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setState = function(value) {
  return jspb.Message.setProto3EnumField(this, 12, value);
};


/**
 * optional Shift shift = 13;
 * @return {!proto.pb.Cycle.Shift}
 */
proto.pb.Cycle.prototype.getShift = function() {
  return /** @type {!proto.pb.Cycle.Shift} */ (jspb.Message.getFieldWithDefault(this, 13, 0));
};


/**
 * @param {!proto.pb.Cycle.Shift} value
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.setShift = function(value) {
  return jspb.Message.setProto3EnumField(this, 13, value);
};


/**
 * optional Upload upload = 14;
 * @return {?proto.pb.Upload}
 */
proto.pb.Cycle.prototype.getUpload = function() {
  return /** @type{?proto.pb.Upload} */ (
    jspb.Message.getWrapperField(this, proto_load_pb.Upload, 14));
};


/**
 * @param {?proto.pb.Upload|undefined} value
 * @return {!proto.pb.Cycle} returns this
*/
proto.pb.Cycle.prototype.setUpload = function(value) {
  return jspb.Message.setWrapperField(this, 14, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.clearUpload = function() {
  return this.setUpload(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.pb.Cycle.prototype.hasUpload = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional Download download = 15;
 * @return {?proto.pb.Download}
 */
proto.pb.Cycle.prototype.getDownload = function() {
  return /** @type{?proto.pb.Download} */ (
    jspb.Message.getWrapperField(this, proto_load_pb.Download, 15));
};


/**
 * @param {?proto.pb.Download|undefined} value
 * @return {!proto.pb.Cycle} returns this
*/
proto.pb.Cycle.prototype.setDownload = function(value) {
  return jspb.Message.setWrapperField(this, 15, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.clearDownload = function() {
  return this.setDownload(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.pb.Cycle.prototype.hasDownload = function() {
  return jspb.Message.getField(this, 15) != null;
};


/**
 * optional Material material = 16;
 * @return {?proto.pb.Material}
 */
proto.pb.Cycle.prototype.getMaterial = function() {
  return /** @type{?proto.pb.Material} */ (
    jspb.Message.getWrapperField(this, proto_material_pb.Material, 16));
};


/**
 * @param {?proto.pb.Material|undefined} value
 * @return {!proto.pb.Cycle} returns this
*/
proto.pb.Cycle.prototype.setMaterial = function(value) {
  return jspb.Message.setWrapperField(this, 16, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.clearMaterial = function() {
  return this.setMaterial(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.pb.Cycle.prototype.hasMaterial = function() {
  return jspb.Message.getField(this, 16) != null;
};


/**
 * optional Truck truck = 17;
 * @return {?proto.pb.Truck}
 */
proto.pb.Cycle.prototype.getTruck = function() {
  return /** @type{?proto.pb.Truck} */ (
    jspb.Message.getWrapperField(this, proto_truck_pb.Truck, 17));
};


/**
 * @param {?proto.pb.Truck|undefined} value
 * @return {!proto.pb.Cycle} returns this
*/
proto.pb.Cycle.prototype.setTruck = function(value) {
  return jspb.Message.setWrapperField(this, 17, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.clearTruck = function() {
  return this.setTruck(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.pb.Cycle.prototype.hasTruck = function() {
  return jspb.Message.getField(this, 17) != null;
};


/**
 * optional Excavator excavator = 18;
 * @return {?proto.pb.Excavator}
 */
proto.pb.Cycle.prototype.getExcavator = function() {
  return /** @type{?proto.pb.Excavator} */ (
    jspb.Message.getWrapperField(this, proto_excavator_pb.Excavator, 18));
};


/**
 * @param {?proto.pb.Excavator|undefined} value
 * @return {!proto.pb.Cycle} returns this
*/
proto.pb.Cycle.prototype.setExcavator = function(value) {
  return jspb.Message.setWrapperField(this, 18, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.clearExcavator = function() {
  return this.setExcavator(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.pb.Cycle.prototype.hasExcavator = function() {
  return jspb.Message.getField(this, 18) != null;
};


/**
 * repeated TruckInfo truckInfo = 19;
 * @return {!Array<!proto.pb.TruckInfo>}
 */
proto.pb.Cycle.prototype.getTruckinfoList = function() {
  return /** @type{!Array<!proto.pb.TruckInfo>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto_streaming_pb.TruckInfo, 19));
};


/**
 * @param {!Array<!proto.pb.TruckInfo>} value
 * @return {!proto.pb.Cycle} returns this
*/
proto.pb.Cycle.prototype.setTruckinfoList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 19, value);
};


/**
 * @param {!proto.pb.TruckInfo=} opt_value
 * @param {number=} opt_index
 * @return {!proto.pb.TruckInfo}
 */
proto.pb.Cycle.prototype.addTruckinfo = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 19, opt_value, proto.pb.TruckInfo, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.pb.Cycle} returns this
 */
proto.pb.Cycle.prototype.clearTruckinfoList = function() {
  return this.setTruckinfoList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.pb.CycleRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.pb.CycleRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.pb.CycleRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.pb.CycleRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    cycle: (f = msg.getCycle()) && proto.pb.Cycle.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.pb.CycleRequest}
 */
proto.pb.CycleRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.pb.CycleRequest;
  return proto.pb.CycleRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.pb.CycleRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.pb.CycleRequest}
 */
proto.pb.CycleRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.pb.Cycle;
      reader.readMessage(value,proto.pb.Cycle.deserializeBinaryFromReader);
      msg.setCycle(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.pb.CycleRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.pb.CycleRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.pb.CycleRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.pb.CycleRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCycle();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.pb.Cycle.serializeBinaryToWriter
    );
  }
};


/**
 * optional Cycle cycle = 1;
 * @return {?proto.pb.Cycle}
 */
proto.pb.CycleRequest.prototype.getCycle = function() {
  return /** @type{?proto.pb.Cycle} */ (
    jspb.Message.getWrapperField(this, proto.pb.Cycle, 1));
};


/**
 * @param {?proto.pb.Cycle|undefined} value
 * @return {!proto.pb.CycleRequest} returns this
*/
proto.pb.CycleRequest.prototype.setCycle = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.pb.CycleRequest} returns this
 */
proto.pb.CycleRequest.prototype.clearCycle = function() {
  return this.setCycle(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.pb.CycleRequest.prototype.hasCycle = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.pb.CycleResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.pb.CycleResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.pb.CycleResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.pb.CycleResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    cycle: (f = msg.getCycle()) && proto.pb.Cycle.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.pb.CycleResponse}
 */
proto.pb.CycleResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.pb.CycleResponse;
  return proto.pb.CycleResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.pb.CycleResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.pb.CycleResponse}
 */
proto.pb.CycleResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.pb.Cycle;
      reader.readMessage(value,proto.pb.Cycle.deserializeBinaryFromReader);
      msg.setCycle(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.pb.CycleResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.pb.CycleResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.pb.CycleResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.pb.CycleResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCycle();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.pb.Cycle.serializeBinaryToWriter
    );
  }
};


/**
 * optional Cycle cycle = 1;
 * @return {?proto.pb.Cycle}
 */
proto.pb.CycleResponse.prototype.getCycle = function() {
  return /** @type{?proto.pb.Cycle} */ (
    jspb.Message.getWrapperField(this, proto.pb.Cycle, 1));
};


/**
 * @param {?proto.pb.Cycle|undefined} value
 * @return {!proto.pb.CycleResponse} returns this
*/
proto.pb.CycleResponse.prototype.setCycle = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.pb.CycleResponse} returns this
 */
proto.pb.CycleResponse.prototype.clearCycle = function() {
  return this.setCycle(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.pb.CycleResponse.prototype.hasCycle = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.pb.CyclesResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.pb.CyclesResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.pb.CyclesResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.pb.CyclesResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.pb.CyclesResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    cyclesList: jspb.Message.toObjectList(msg.getCyclesList(),
    proto.pb.Cycle.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.pb.CyclesResponse}
 */
proto.pb.CyclesResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.pb.CyclesResponse;
  return proto.pb.CyclesResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.pb.CyclesResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.pb.CyclesResponse}
 */
proto.pb.CyclesResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.pb.Cycle;
      reader.readMessage(value,proto.pb.Cycle.deserializeBinaryFromReader);
      msg.addCycles(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.pb.CyclesResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.pb.CyclesResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.pb.CyclesResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.pb.CyclesResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCyclesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.pb.Cycle.serializeBinaryToWriter
    );
  }
};


/**
 * repeated Cycle cycles = 1;
 * @return {!Array<!proto.pb.Cycle>}
 */
proto.pb.CyclesResponse.prototype.getCyclesList = function() {
  return /** @type{!Array<!proto.pb.Cycle>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.pb.Cycle, 1));
};


/**
 * @param {!Array<!proto.pb.Cycle>} value
 * @return {!proto.pb.CyclesResponse} returns this
*/
proto.pb.CyclesResponse.prototype.setCyclesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.pb.Cycle=} opt_value
 * @param {number=} opt_index
 * @return {!proto.pb.Cycle}
 */
proto.pb.CyclesResponse.prototype.addCycles = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.pb.Cycle, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.pb.CyclesResponse} returns this
 */
proto.pb.CyclesResponse.prototype.clearCyclesList = function() {
  return this.setCyclesList([]);
};


goog.object.extend(exports, proto.pb);
